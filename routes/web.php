<?php

use App\Http\Controllers\buy;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\profile;
use App\Http\Controllers\purchase;
use App\Http\Controllers\sales;
use App\Http\Controllers\sell;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PembeliController;
use App\Http\Controllers\PenjualController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UsulanController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\App;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//admin
Route::get('/admin', function () {
    return view('layout/master');
});

Route::get('/', [DashboardController::class, 'index']);
Route::get('/dashboard', [DashboardController::class, 'index']);

Route::get('/indexpembeli', [PembeliController::class, 'index']);
Route::get('/indexpenjual', [PenjualController::class, 'index']);
Route::get('/indexproduk', [ProdukController::class, 'index']);
Route::get('/indextransaksi', [TransaksiController::class, 'index']);
Route::get('/indexusulan', [UsulanController::class, 'index']);
Route::get('/indexlaporan', [LaporanController::class, 'index']);


//category
route::get('categories', [CategoryController::class, 'index']);
Route::get('categories/{$category}', [CategoryController::class, 'show']);
route::post('categories', [CategoryController::class, 'store']);
route::put('categories/{$category}', [CategoryController::class, 'update']);
route::delete('categories/{$category}', [CategoryController::class, 'destroy']);

//subcategory
route::get('subcategory', [CategoryController::class, 'index']);
Route::get('subcategory/{$subcategory}', [CategoryController::class, 'show']);
route::post('subcategory', [CategoryController::class, 'store']);
route::put('subcategory/{$subcategory}', [CategoryController::class, 'update']);
route::delete('subcategory/{$subcategory}', [CategoryController::class, 'destroy']);

//user
route::get('/buy', [buy::class, 'index']);
Route::get('/sell', [sell::class, 'index']);
route::get('/profile', [profile::class, 'index']);
route::get('/purchase', [purchase::class, 'index']);
route::get('/sales', [sales::class, 'index']);

//register
Route::get('/indexregister', [RegisterController::class, 'index']);
