<?php

use App\Http\Controllers\authcontroller;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProduksController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\SubcategoryController;
use App\Http\Controllers\TestimoniController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
    "middleware" => 'api',
    "prefix" => "auth",
], function () {
    Route::post('login', [authcontroller::class, 'login'])->name('login');
});

route::group([
    "middleware" => 'api',
],function (){
    Route::resources([
        'categories' => CategoryController::class ,
        'subcategories' => SubcategoryController::class,
        'barangs' => BarangController::class,
        'testimoni' => TestimoniController::class,
        'review' => ReviewController::class,
    ]);
});


