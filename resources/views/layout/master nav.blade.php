<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

  <!-- General CSS Files -->
  <link rel="stylesheet" href="template1/dist/assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="template1/dist/assets/modules/fontawesome/css/all.min.css">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="template1/dist/assets/css/style.css">
  <link rel="stylesheet" href="template1/dist/assets/css/components.css">
  <!-- Start GA -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-94034622-3');
  </script>
  <!-- /END GA -->
</head>

<body class="layout-3">
  <div id="app">
    <div class="main-wrapper container">
      <div class="navbar-bg"></div>

      <nav class="navbar navbar-expand-lg main-navbar">
        <a href="index.html" class="navbar-brand sidebar-gone-hide"><img src="template1\dist\assets\img\fixlogonobg.png" alt="Logo" width="60" height=" 60">SOURCECODE</a>
        <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
        <div class="nav-collapse">
          <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
            <i class="fas fa-ellipsis-v"></i>
          </a>
          <ul class="navbar-nav container">
            <li class="nav-item {{ ($title == "Buy") ? 'active' : '' }}"><a href="buy" class="nav-link ">Buy</a></li>
            <li class="nav-item  {{ ($title == "Sell") ? 'active' : '' }}"><a href="sell" class="nav-link ">Sell</a></li>
          </ul>
        </div>
        <form class="form-inline ml-auto">
          <ul class="navbar-nav">
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link nav-link-lg message-toggle beep"><i class="far fa-envelope"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
              <div class="dropdown-list-content dropdown-list-message">
                <div class="dropdown-header">Messages</div>
                <a href="#" class="dropdown-item dropdown-item-unread"></a>
              </div>
          </li>
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
              <div class="dropdown-header">Notifications</div>
              <div class="dropdown-list-content dropdown-list-icons">
                <a href="#" class="dropdown-item dropdown-item-unread"></a>
              </div>
            </div>
          </li>
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              <img alt="image" src="template1/dist/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
              <div class="d-sm-none d-lg-inline-block">Hi, Ujang Maman</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="profile" class="dropdown-item has-icon">
                <i class="far fa-user {{ ($title == "Profile") ? 'active' : '' }}"></i> Profile
              </a>
              <a href="#" class="dropdown-item has-icon">
                <i class="fas fa-home"></i> Dashboard
              </a>
              <a href="purchase" class="dropdown-item has-icon">
                <i class="fas fa-money-bill"></i> Purchase History
              </a>
              <a href="sales" class="dropdown-item has-icon">
                <i class="fas fa-history"></i> Sales History
              </a>
              <a href="#" class="dropdown-item has-icon">
                <i class="fas fa-wallet"></i> Wallet
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>

      <nav class="navbar navbar-secondary navbar-expand-lg">
        <div class="container">
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link"><i class="fab fa-php"></i><span>PHP</span></a>
            </li>
            <li class="nav-item dropdown">
              <a href="#" class="nav-link"><i class="fas fa-file-alt"></i><span>Template</span></a>
            </li>
            <li class="nav-item dropdown">
              <a href="#" class="nav-link"><i class="fab fa-java"></i><span>Javascript</span></a>
            </li>
          </ul>
        </div>
      </nav>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>{{ $name }}</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="#">Layout</a></div>
              <div class="breadcrumb-item">{{ $name }}</div>
            </div>
          </div>

          @yield('content')

        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2023 <div class="bullet"></div> Design By Kelompok SOURCECODE
        </div>
        <div class="footer-right">

        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="template1/dist/assets/modules/jquery.min.js"></script>
  <script src="template1/dist/assets/modules/popper.js"></script>
  <script src="template1/dist/assets/modules/tooltip.js"></script>
  <script src="template1/dist/assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="template1/dist/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="template1/dist/assets/modules/moment.min.js"></script>
  <script src="template1/dist/assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Page Specific JS File -->

  <!-- Template JS File -->
  <script src="template1/dist/assets/js/scripts.js"></script>
  <script src="template1/dist/assets/js/custom.js"></script>
</body>

</html>