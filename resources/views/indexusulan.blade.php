@extends('layout.master')
@section('content')
<div class="section-header">
  <h1>Recap Usulan</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
    <div class="breadcrumb-item"><a href="#">Recap</a></div>
    <div class="breadcrumb-item">TableRecapUsulan</div>
  </div>
</div>
<div class="nav justify-content-end">
  <button class="btn active btn-info">Tambah Data</button>
</div>
<div class="row">
  <div class="col-sm-12 col-md-6">
    <div class="dataTables_length" id="table-1_length">
      <label>Show entries:
        <select name="table-1_length" aria-controls="table-1" class="form-control form-control-sm">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
      </label>
    </div>
  </div>
  <div class="col-sm-12 col-md-6">
    <div id="table-1_filter" class="dataTables_filter">
      <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="table-1" /></label>
    </div>
  </div>
</div>
<div class="card">
  <div class="card-header">
    <h4>Simple Table</h4>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-md">
        <tbody>
          <tr style="background-color: #87CEFA">
            <th class="text-center">No</th>
            <th class="text-center">Penjual</th>
            <th class="text-center">Pembeli</th>
            <th class="text-center">Tanggal Terjual</th>
            <th class="text-center">Id Produk</th>
            <th class="text-center">Produk</th>
            <th class="text-center">Action</th>
          </tr>
          <tr>
            <td class="text-center">1</td>
            <td class="text-center">Dinda</td>
            <td class="text-center">indah</td>
            <td class="text-center">2017-01-09</td>
            <td class="text-center">0001</td>
            <td class="text-center">hanphone</td>
            <td class="text-center"><a href="#" class=" btn btn-info">Show</a>
              <a href="#" class=" btn btn-warning" style="text-align: center;">Edit</a>
              <a href="#" class=" btn btn-danger" style="text-align: center;">Delete</a>
            </td>
          </tr>
          <tr>
            <td class="text-center">2</td>
            <td class="text-center">dwi</td>
            <td class="text-center">dinda</td>
            <td class="text-center">2017-01-09</td>
            <td class="text-center">0001</td>
            <td class="text-center">hanphone</td>
            <td class="text-center"><a href="#" class=" btn btn-info">Show</a>
              <a href="#" class="btn btn-warning">Edit</a>
              <a href="#" class="btn btn-danger">Delete</a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer text-right">
    <nav class="d-inline-block">
      <ul class="pagination mb-0">
        <li class="page-item disabled">
          <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
        </li>
        <li class="page-item "><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
        <li class="page-item">
          <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
          <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
        </li>
      </ul>
    </nav>
  </div>
</div>
@endsection