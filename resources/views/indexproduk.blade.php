@extends('layout.master')
@section('content')

<div class="section-header">
    <h1>Daftar Produk </h1>
</div>
<div class="col-12 ">
    <div class="card">

        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tbody>
                        <tr>
                            <th style="text-align: center; font-weight:bold">No</th>
                            <th style="text-align: center; font-weight:bold">Nama Produk</th>
                            <th style="text-align: center; font-weight:bold">Deskripsi</th>
                            <th style="text-align: center; font-weight:bold">Tanggal Upload</th>
                            <th style="text-align: center; font-weight:bold">Harga</th>
                            <th style="text-align: center; font-weight:bold">Action</th>
                        </tr>
                        <tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>
                                <div class="d-flex justify-content-center">
                                    <a href="#" class="btn btn-info m-2">Show</a>
                                    <a href="#" class="btn btn-primary m-2">Edit</a>
                                    <form action="#" method="POST">

                                        <input type="submit" name="delete" class=" deleteButton btn btn-danger my-1 m-2" value="Delete">
                                    </form>
                                </div>

                                </form>
                            </td>
                        </tr>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer text-right">
            <nav class="d-inline-block">
                <ul class="pagination mb-0">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
@endsection