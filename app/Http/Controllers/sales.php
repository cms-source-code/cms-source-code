<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class sales extends Controller
{
    public function index()
    {
        return view("sales",[
            "title"=>"Sales History",
            "name"=> "Sales History",
        ]);
    }
}
