<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authcontroller extends Controller
{
    public function login()
    {
        $credentials = request(["email", "password"]);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(["Email password is wrong"], 401);
        }

        return $this->respondwithtoken($token);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
